-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2021 at 03:27 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(8) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `spesifikasi` varchar(35) NOT NULL,
  `lokasi_barang` varchar(40) NOT NULL,
  `kategori` varchar(25) NOT NULL,
  `total_barang` int(11) NOT NULL,
  `kondisi` int(2) NOT NULL,
  `jenis_barang` varchar(20) NOT NULL,
  `sumber_dana` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `spesifikasi`, `lokasi_barang`, `kategori`, `total_barang`, `kondisi`, `jenis_barang`, `sumber_dana`) VALUES
('MB1', 'Motherboard Asal', 'Baru', 'Kelas', 'Hardware', 10, 1, 'Motherboard', 'Hamba'),
('MB2', 'Motherboard', 'asd', '123', 'mb', 3, 0, 'mb', 'mb'),
('MB4', 'Asrock Sniper', 'Motherboard Bagus', 'Batam', 'Hardware', 12, 1, 'Motherboard', 'domba tersesat'),
('qweqe', 'LoJitak', 'mayan lah', 'tangsel', 'atribut', 21, 1, 'Mos', 'domba tersesat');

-- --------------------------------------------------------

--
-- Table structure for table `keluar_barang`
--

CREATE TABLE `keluar_barang` (
  `id_brg_keluar` int(11) NOT NULL,
  `kode_barang` varchar(8) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `penerima` varchar(35) NOT NULL,
  `jml_brg_keluar` int(8) NOT NULL,
  `jml_brg_keluar_old` int(11) NOT NULL,
  `keperluan` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keluar_barang`
--

INSERT INTO `keluar_barang` (`id_brg_keluar`, `kode_barang`, `tgl_keluar`, `penerima`, `jml_brg_keluar`, `jml_brg_keluar_old`, `keperluan`) VALUES
(1, 'MB1', '2019-04-27', 'Wahyu', 1, 1, 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `masuk_barang`
--

CREATE TABLE `masuk_barang` (
  `id_brg_masuk` int(11) NOT NULL,
  `kode_barang` varchar(8) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jml_brg_masuk` int(8) NOT NULL,
  `jml_brg_masuk_old` int(11) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masuk_barang`
--

INSERT INTO `masuk_barang` (`id_brg_masuk`, `kode_barang`, `tgl_masuk`, `jml_brg_masuk`, `jml_brg_masuk_old`, `kode_supplier`) VALUES
(6, 'MB1', '2019-04-26', 10, 10, 'AP1');

-- --------------------------------------------------------

--
-- Table structure for table `pinjam_barang`
--

CREATE TABLE `pinjam_barang` (
  `id_brg_pinjam` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `kode_barang` varchar(8) NOT NULL,
  `jml_pinjam` int(8) NOT NULL,
  `jml_pinjam_old` int(8) NOT NULL,
  `peminjaman` varchar(35) NOT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjam_barang`
--

INSERT INTO `pinjam_barang` (`id_brg_pinjam`, `tgl_pinjam`, `kode_barang`, `jml_pinjam`, `jml_pinjam_old`, `peminjaman`, `tgl_kembali`, `keterangan`) VALUES
(8, '2019-04-27', 'MB1', 1, 1, 'wahyu', '2019-04-27', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(35) NOT NULL,
  `alamat_supplier` varchar(50) NOT NULL,
  `telp_supplier` varchar(20) NOT NULL,
  `kota_supplier` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat_supplier`, `telp_supplier`, `kota_supplier`) VALUES
('AP1', 'UDIIIIN', 'Tangerang', '021123123', 'Tangerang'),
('SD2', 'SD Negeri 01', 'Pondok Aren, Tangerang Selatan', '021232323', 'Pondok Aren'),
('SD3', 'SD Negeri 04 Jakarta', 'Samping Masjid, Jakarta', '0214553342', 'Jakarta'),
('SSD', 'Jacky', 'Rumahnya Jacky', '08177777777', 'Jackartha'),
('www', 'Samsudin', 'bengawan solo', '0812345678', 'WONOSOBO');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(40) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `level`) VALUES
(1, 'Manajer', 'Admin', '$2y$10$jV8WiCMBNIq3CmY.Nt8QQu9lqllZkRCJy', 1),
(2, 'Administrator', 'adm', '$2y$10$mnWOIFH3wU5tECCvRa9/turMnWJ3eC8p7', 3),
(3, 'super user', 'sudo', '$2y$10$mnWOIFH3wU5tECCvRa9/turMnWJ3eC8p7', 3),
(4, 'BAGAS', 'bagaskun123', '$2y$10$mnWOIFH3wU5tECCvRa9/turMnWJ3eC8p7', 3),
(5, 'BAGAS', 'bagaskun', '$2y$10$mnWOIFH3wU5tECCvRa9/turMnWJ3eC8p7', 3),
(21, 'SAMSUDIN', 'samsung', '$2y$10$5TWpUqiEkPA0BeAyD28K7.1wo3pe1bTCE', 2);

-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `userIp` varbinary(16) NOT NULL,
  `loginTime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlog`
--

INSERT INTO `userlog` (`id`, `id_user`, `userIp`, `loginTime`) VALUES
(4, 1, 0x3a3a31, '2019-04-26 14:51:59'),
(5, 2, 0x3a3a31, '2019-04-26 14:52:15'),
(6, 1, 0x3a3a31, '2019-04-26 14:52:22'),
(7, 1, 0x3a3a31, '2019-04-27 00:15:35'),
(8, 1, 0x3a3a31, '2019-04-27 00:23:01'),
(9, 2, 0x3a3a31, '2019-04-27 00:23:18'),
(10, 3, 0x3a3a31, '2019-04-27 00:26:15'),
(11, 1, 0x3a3a31, '2019-04-27 00:42:04'),
(12, 1, 0x3a3a31, '2019-04-27 00:58:14'),
(13, 3, 0x3a3a31, '2019-04-27 01:25:59'),
(14, 1, 0x3a3a31, '2019-04-27 01:36:39'),
(15, 2, 0x3a3a31, '2019-04-27 01:42:57'),
(16, 2, 0x3a3a31, '2019-04-27 01:49:09'),
(17, 1, 0x3a3a31, '2019-04-27 01:49:39'),
(18, 1, 0x3a3a31, '2019-04-27 02:21:02'),
(19, 1, 0x3a3a31, '2019-04-27 04:11:15'),
(20, 1, 0x3a3a31, '2019-04-27 04:16:53'),
(21, 3, 0x3a3a31, '2019-04-27 04:34:28'),
(22, 3, 0x3a3a31, '2019-04-27 04:35:06'),
(23, 1, 0x3a3a31, '2019-04-27 04:48:21'),
(24, 1, 0x3a3a31, '2019-04-27 05:22:36'),
(25, 1, 0x3a3a31, '2019-04-27 05:44:38'),
(26, 3, 0x3a3a31, '2019-04-27 05:53:12'),
(27, 1, 0x3a3a31, '2019-04-27 05:54:44'),
(28, 2, 0x3a3a31, '2019-04-27 05:54:53'),
(29, 1, 0x3a3a31, '2019-04-27 05:55:16'),
(30, 3, 0x3a3a31, '2019-04-27 05:56:07'),
(31, 2, 0x3a3a31, '2019-04-27 06:16:09'),
(32, 1, 0x3a3a31, '2019-04-27 06:17:24'),
(33, 2, 0x3a3a31, '2019-04-27 06:18:25'),
(34, 1, 0x3a3a31, '2019-04-27 06:19:34'),
(35, 1, 0x3132372e302e302e31, '2021-01-15 09:33:01'),
(36, 1, 0x3132372e302e302e31, '2021-01-15 09:35:07'),
(37, 1, 0x3132372e302e302e31, '2021-01-16 03:28:11'),
(38, 1, 0x3132372e302e302e31, '2021-01-20 06:53:14'),
(39, 1, 0x3a3a31, '2021-10-08 21:57:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `keluar_barang`
--
ALTER TABLE `keluar_barang`
  ADD PRIMARY KEY (`id_brg_keluar`),
  ADD KEY `kode_barang` (`kode_barang`);

--
-- Indexes for table `masuk_barang`
--
ALTER TABLE `masuk_barang`
  ADD PRIMARY KEY (`id_brg_masuk`),
  ADD KEY `kode_barang` (`kode_barang`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  ADD PRIMARY KEY (`id_brg_pinjam`),
  ADD KEY `kode_barang` (`kode_barang`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keluar_barang`
--
ALTER TABLE `keluar_barang`
  MODIFY `id_brg_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `masuk_barang`
--
ALTER TABLE `masuk_barang`
  MODIFY `id_brg_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  MODIFY `id_brg_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `keluar_barang`
--
ALTER TABLE `keluar_barang`
  ADD CONSTRAINT `keluar_barang_ibfk_1` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`);

--
-- Constraints for table `masuk_barang`
--
ALTER TABLE `masuk_barang`
  ADD CONSTRAINT `masuk_barang_ibfk_1` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`),
  ADD CONSTRAINT `masuk_barang_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`);

--
-- Constraints for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  ADD CONSTRAINT `pinjam_barang_ibfk_1` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`);

--
-- Constraints for table `userlog`
--
ALTER TABLE `userlog`
  ADD CONSTRAINT `userlog_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
