<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

	public function index()
	{
		$data['user'] = $this->User_model->getAll();
		$this->load->view('includes/header');
		$this->load->view('User',$data);	
		$this->load->view('includes/footer');
	}

	public function edit_user($id_user)
	{
		$data['user'] = $this->User_model->getById($id_user);
		$this->load->view('./includes/header');
		$this->load->view('action/editUser',$data);	
		$this->load->view('./includes/footer');	
	}

	public function tambah_user()
	{
		$data['user'] = $this->User_model->getAll();
		$this->load->view('./includes/header');
		$this->load->view('action/tambahUser',$data);	
		$this->load->view('./includes/footer');	
	}

	public function hapus_user($id_user)
	{
		$this->User_model->Delete($id_user);
		redirect('User');
	}

	public function proses_tambah_User()
	{
		$this->User_model->Add();
		redirect('User');
	}

	public function proses_edit_User()
	{
		$this->User_model->Edit();
		redirect('User');
	}

	public function search_user()
	{	
		$cari = $this->input->GET('cari', TRUE);
		$data['user'] = $this->User_model->Search($cari);
		$this->load->view('./includes/header');
		$this->load->view('search/Hasil_user', $data);
		$this->load->view('./includes/footer');	
	}
}
