<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Supplier_model');
	}

	public function index()
	{
		$data['supplier'] = $this->Supplier_model->getAll();
		$this->load->view('includes/header');
		$this->load->view('supplier',$data);	
		$this->load->view('includes/footer');
	}

	public function edit_supplier($kode_supplier)
	{
		$data['supplier'] = $this->Supplier_model->getById($kode_supplier);
		$this->load->view('./includes/header');
		$this->load->view('action/editSupplier',$data);	
		$this->load->view('./includes/footer');	
	}

	public function tambah_supplier()
	{
		$data['supplier'] = $this->Supplier_model->getAll();
		$this->load->view('./includes/header');
		$this->load->view('action/tambahSupplier',$data);	
		$this->load->view('./includes/footer');	
	}

	public function hapus_supplier($kode_supplier)
	{
		$this->Supplier_model->Delete($kode_supplier);
		redirect('supplier');
	}

	public function proses_tambah_supplier()
	{
		$this->Supplier_model->Add();
		redirect('supplier');
	}

	public function proses_edit_supplier()
	{
		$this->Supplier_model->Edit();
		redirect('supplier');
	}

	public function search_supplier()
	{	
		$cari = $this->input->GET('cari', TRUE);
		$data['supplier'] = $this->Supplier_model->Search($cari);
		$this->load->view('./includes/header');
		$this->load->view('search/Hasil_supplier', $data);
		$this->load->view('./includes/footer');	
	}
}
