<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_model');
	}

	public function index()
	{
		$data['barang'] = $this->Barang_model->getAll();
		$this->load->view('includes/header');
		$this->load->view('barang',$data);	
		$this->load->view('includes/footer');
	}

	public function edit_barang($kode_barang)
	{
		$data['barang'] = $this->Barang_model->getById($kode_barang);
		$this->load->view('./includes/header');
		$this->load->view('action/editBarang',$data);	
		$this->load->view('./includes/footer');	
	}

	public function tambah_barang()
	{
		$data['barang'] = $this->Barang_model->getAll();
		$this->load->view('./includes/header');
		$this->load->view('action/tambahBarang',$data);	
		$this->load->view('./includes/footer');	
	}

	public function hapus_barang($kode_barang)
	{
		$this->Barang_model->Delete($kode_barang);
		redirect('barang');
	}

	public function proses_tambah_barang()
	{
		$this->Barang_model->Add();
		redirect('barang');
	}

	public function proses_edit_barang()
	{
		$this->Barang_model->Edit();
		redirect('barang');
	}
 
	public function search_barang()
	{	
		$cari = $this->input->GET('cari', TRUE);
		$data['barang'] = $this->Barang_model->Search($cari);
		$this->load->view('./includes/header');
		$this->load->view('search/Hasil_barang', $data);
		$this->load->view('./includes/footer');	
	}
}
