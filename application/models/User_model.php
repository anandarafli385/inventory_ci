<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model 
{
	public function getAll()
	{
		return $this->db->get('user')->result_array();
	}

	public function getById($id_user)
	{
		return $this->db->get_where('user',['id_user'=>$id_user])->row_array();
	}

	public function Add()
	{
			$nama = $this->input->post("nama",true);
			$username = $this->input->post("username",true);
			$password = $this->input->post("password", true);
			$level = $this->input->post("level",true);	
		$data = [
			"nama" => $nama,
			"username" => $username,
			"password" => password_hash($password, PASSWORD_BCRYPT),
			"level" => $level,
		];

		$this->db->insert('user', $data);
	}

	public function Delete($id_user)
	{
		$this->db->where('id_user',$id_user);
		$this->db->delete('user');
	}

	public function Edit()
	{
			$password = $this->input->post('password',true);
		$data = [
			"nama" => $this->input->post('nama'),
			"username" => $this->input->post('username'),
			"password" => password_hash($password, PASSWORD_BCRYPT),
			"level" => $this->input->post('level'),
		];

		$this->db->where('id_user',$this->input->post('id_user'));
		$this->db->update('user',$data);
	}

	public function Search($cari)
	{
		$data = $this->db->query("SELECT * FROM user 
                       WHERE nama LIKE '%".$cari."%' OR
                       username LIKE '%".$cari."%' OR
                       level LIKE '%".$cari."%'
                      ORDER BY nama DESC");
		return $data->result_array();
	}
}
