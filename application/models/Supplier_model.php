<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model 
{
	public function getAll()
	{
		return $this->db->get('supplier')->result_array();
	}

	public function getById($kode_supplier)
	{
		return $this->db->get_where('supplier',['kode_supplier'=>$kode_supplier])->row_array();
	}

	public function Add()
	{
		$data = [
			"kode_supplier" => $this->input->post('kode_supplier'),
			"nama_supplier" => $this->input->post('nama_supplier'),
			"telp_supplier" => $this->input->post('telp_supplier'),
			"kota_supplier" => $this->input->post('kota_supplier'),
			"alamat_supplier" => $this->input->post('alamat_supplier'),
		];

		$this->db->insert('supplier', $data);
	}

	public function Delete($kode_supplier)
	{
		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->delete('supplier');
	}

	public function Edit()
	{
		$data = [
			"nama_supplier" => $this->input->post('nama_supplier'),
			"telp_supplier" => $this->input->post('telp_supplier'),
			"kota_supplier" => $this->input->post('kota_supplier'),
			"alamat_supplier" => $this->input->post('alamat_supplier'),
		];

		$this->db->where('kode_supplier',$this->input->post('kode_supplier'));
		$this->db->update('supplier',$data);
	}

	public function Search($cari)
	{
		$data = $this->db->query("SELECT * FROM supplier 
                       WHERE kode_supplier LIKE '%".$cari."%' OR 
                       nama_supplier LIKE '%".$cari."%' OR 
                       alamat_supplier LIKE '%".$cari."%' OR
                       telp_supplier LIKE '%".$cari."%' OR 
                       kota_supplier LIKE '%".$cari."%' 
                       ORDER BY kode_supplier DESC");
		return $data->result_array();
	}
}
