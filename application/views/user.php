	<main role="main">
	  <div class="container-fluid">
		<div class="card">
		  <div class="card-body">
		  	<h4>User Database</h4>
			<div class="float-right pb-5">
				<a class="btn btn-outline-info" href="<?= base_url('barang')?>">Barang</a>
				<a class="btn btn-outline-info" href="<?= base_url('supplier')?>">Supplier</a>
				<a class="btn btn-outline-success d-flex mt-2 px-4 mx-2" href="<?= base_url('user/tambah_user')?>"> Register User</a>
			</div>
			<form class="form-inline" method="get" action="<?= base_url('user/search_user')?>">
				<input class="form-control mr-sm-2" type="text" placeholder="Search User" name="cari" required>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
			</form>
			<div class="table-responsive">
			  <table class="table table-bordered">
				<thead>
				  <tr>
				    <td>No</td>
				    <td>Nama</td>
				    <td>Username</td>
				    <td>Password</td>
				    <td>Level User</td>
				    <td>Setting</td>
				  </tr>
				</thead>
				<tbody>
          <?php 
          foreach ($user as $usr) : 
          ?>
            <tr>
              <td><?=$usr['id_user']?></td> <!-- Id Pengguna -->
              <td><?=$usr['username']?></td> <!--Nama Pengguna-->
              <td><?=$usr['nama'] ?></td> <!--Username Pengguna -->
              <td><?=$usr['password']?> </td> <!-- Password(Hash) Pengguna -->
              <td><?=$usr['level'] ?></td> <!-- Level Akses Pengguna -->
              <td>
                <div class="btn-group">
                  <a class="btn btn-outline-info" href="<?= base_url()?>user/edit_user/<?= $usr['id_user']; ?>">Edit</a>
                  <a class="btn btn-outline-danger" href="<?= base_url()?>user/hapus_user/<?= $usr['id_user']; ?>" onclick="return confirm('Apakah anda yakin menghapus data dengan nama <?=$usr['nama']?>')">Hapus</a>
                </div>
              </td>
            </tr>
          <?php endforeach; ?>
				</tbody>
			  </table>
			</div>
		  </div>
		</div>
	  </div>
	</main>