<!doctype html>
<html lang ="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Inventory</title>
		<link href="<?php echo base_url('assets/css/main.css');?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
				<a class="navbar-brand" href="#">INVENTORY</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarCollapse" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse float-right" id="navbar">
					<ul class="navbar-nav ml-auto">
						<marquee style="color: #fff; text-transform: uppercase;">selamat datang di aplikasi inventory</marquee>
					</ul>
				</div>
			</nav>
		</header>
