     <main class="main">
       <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <h4>Tambah Data</h4>
            <div class="float-right" style="padding-bottom:20px;">
              <a class="btn btn-outline-dark" href="<?= base_url('barang')?>">Kembali</a>
            </div>
            <br><br>
          <form action="<?= base_url('barang/proses_edit_barang') ?>" method="post">
            <div class="form-group">
              <label for="kode_barang">Kode Barang</label>
              <input name="kode_barang" value="<?=$barang['kode_barang'];?>" type="text" readonly maxlength="5" class="form-control" placeholder="Kode Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="nama_barang">Nama Barang</label>
              <input name="nama_barang" value="<?=$barang['nama_barang'];?>" type="text" class="form-control" placeholder="Nama Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="kategori">Kategori</label>
              <input name="kategori" value="<?=$barang['kategori'];?>" type="text" class="form-control" placeholder="Kategori Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="jenis_barang">Jenis Barang</label>
              <input class="form-control" value="<?=$barang['jenis_barang'];?>" type="text" name="jenis_barang"  placeholder="Jenis Barang"autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="lokasi_barang">Lokasi Barang</label>
              <textarea class="form-control" type="text" name="lokasi_barang"  placeholder="Lokasi Barang" autocomplete="off" required><?=$barang['lokasi_barang'];?></textarea>
            </div>
            <div class="form-group">
              <label for="total_barang">Total Barang</label>
              <input class="form-control" value="<?=$barang['total_barang']?>" type="text" name="total_barang"  placeholder="Jenis Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="sumber_dana">Sumber Dana</label>
              <input class="form-control" value="<?=$barang['sumber_dana']?>" type="text" name="sumber_dana"  placeholder="Sumber Dana" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="kondisi">Kondisi Barang</label>
              <div class="col-md-6 mb-3">
                <div class="row">
                  <div class="col-md-4">
                    <input class="form-control" type="radio" name="kondisi" value="1" required><p class="alert-success text-center" muted>Baik</p>
                  </div>
                  
                  <div class="col-md-4">
                    <input class="form-control" type="radio" name="kondisi" value="0" required><p class="alert-info text-center">Kurang Baik</p>
                  </div>
                  
                </div>
            </div>
            <div class="form-group">
              <label for="spesifikasi">Spesifikasi Barang</label>
              <input name="spesifikasi" value="<?=$barang['spesifikasi']?>" type="text" maxlength="15" class="form-control" placeholder="Spesifikasi Barang" autocomplete="off" required>
            </div>
                <button class="btn btn-primary btn-lg btn-block" name="editBarang" type="submit">Simpan Data</button>
                <a class="btn btn-secondary btn-lg btn-block" href="<?= base_url('barang') ?>" >Kembali</a>
          </form>
          </div>
        </div>
      </div>
    </main>