     <main class="main">
       <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <h4>Tambah Data</h4>
            <div class="float-right" style="padding-bottom:20px;">
              <a class="btn btn-outline-dark" href="<?= base_url('supplier')?>">Kembali</a>
            </div>
            <br><br>
            <form method="post" action="<?= base_url('supplier/proses_tambah_supplier') ?>">
            <div class="form-group">
            	<label for="kode_supplier">Kode Supplier</label>
            	<input name="kode_supplier" type="text" maxlength="5" class="form-control" placeholder="Kode Supplier" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="nama_supplier">Nama Supplier</label>
              <input name="nama_supplier" type="text" class="form-control" placeholder="Nama Supplier" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="telp_supplier">No Telepon Supplier</label>
              <input name="telp_supplier" type="number" maxlength="15" class="form-control" placeholder="No Telepon Supplier" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="kota_supplier">Kota Supplier</label>
              <input name="kota_supplier" type="text" class="form-control" placeholder="Kota Supplier" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="alamat_supplier">Alamat Supplier</label>
              <textarea class="form-control" name="alamat_supplier" rows="8" cols="80" placeholder="Alamat Supplier" required></textarea>
            </div>
            <button class="btn btn-primary btn-lg btn-block" name="tambahSupplier" type="submit">Tambah Data</button>
            <button class="btn btn-danger btn-lg btn-block" type="reset" onclick="return confirm('Apakah anda yakin untuk me-reset data tersebut?')">Reset</button>
            <a class="btn btn-secondary btn-lg btn-block" href="supplier.php" >Kembali</a>
          </form>
          </div>
        </div>
      </div>
    </main>