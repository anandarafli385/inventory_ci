     <main class="main">
       <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <h4>Tambah Data</h4>
            <div class="float-right" style="padding-bottom:20px;">
              <a class="btn btn-outline-dark" href="<?= base_url('barang')?>">Kembali</a>
            </div>
            <br><br>
            <form action="<?= base_url('barang/proses_tambah_barang')  ?>" method="post">
            <div class="form-group">
            	<label for="kode_barang">Kode Barang</label>
            	<input name="kode_barang" type="text" maxlength="5" class="form-control" placeholder="Kode Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="nama_barang">Nama Barang</label>
              <input name="nama_barang" type="text" class="form-control" placeholder="Nama Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="kategori">Kategori</label>
              <input name="kategori" type="text" class="form-control" placeholder="Kategori Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="jenis_barang">Jenis Barang</label>
              <input name="jenis_barang" type="text" maxlength="15" class="form-control" placeholder="Spesifikasi Barang" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="lokasi_barang">Lokasi Barang</label>
              <textarea class="form-control" name="lokasi_barang"  placeholder="Lokasi Barang" required></textarea>
            </div>
            <div class="form-group">
              <label for="total_barang">Total Barang</label>
              <input class="form-control" name="total_barang"  placeholder="Jumlah Barang" required></input>
            </div>
            <div class="form-group">
              <label for="sumber_dana">Sumber Dana</label>
              <input class="form-control" name="sumber_dana"  placeholder="Sumber Dana" required></input>
            </div>
            <div class="form-group">
              <label for="kondisi">Kondisi Barang</label>
              <div class="col-md-6 mb-3">
                <div class="row">
                  <div class="col-md-4">
                    <input class="form-control" type="radio" name="kondisi" value="1" required><p class="alert-success text-center" muted>Baik</p>
                  </div>
                  
                  <div class="col-md-4">
                    <input class="form-control" type="radio" name="kondisi" value="0" required><p class="alert-info text-center">Kurang Baik</p>
                  </div>
                  
                </div>
            </div>
            <div class="form-group">
              <label for="spesifikasi">Spesifikasi Barang</label>
              <input name="spesifikasi" type="text" class="form-control" placeholder="Spesifikasi Barang" autocomplete="off" required>
            </div>
            <button class="btn btn-primary btn-lg btn-block" name="tambahBarang" type="submit">Tambah Data</button>
            <button class="btn btn-danger btn-lg btn-block" type="reset" onclick="return confirm('Apakah anda yakin untuk me-reset data tersebut?')">Reset</button>
            <a class="btn btn-secondary btn-lg btn-block" href="<?= base_url('barang')  ?>" >Kembali</a>
          </form>
          </div>
        </div>
      </div>
    </main>