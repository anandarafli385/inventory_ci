     <main class="main">
       <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <h4>Tambah Data</h4>
            <div class="float-right" style="padding-bottom:20px;">
              <a class="btn btn-outline-dark" href="<?= base_url('User')?>">Kembali</a>
            </div>
            <br><br>
          <form action="<?= base_url('User/proses_tambah_user')  ?>" method="post">
            <div class="form-group">
            	<label for="nama">Nama User</label>
            	<input name="nama" type="text" class="form-control" placeholder="Nama User" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="username">Username</label>
              <input name="username" type="text" class="form-control" placeholder="Username Pengguna" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input name="password" type="Password" minlength="8" class="form-control" placeholder="Password" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="konfirmasi_password">Konfirmasi Password</label>
              <input name="konfirmasi_password" type="Password" class="form-control" placeholder="`Konfirmasi Passowrd" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="level">Level User</label>
              <select class="form-control" name="level"  placeholder="Level User" required>
                <option class="text-muted">-Pilih Level User-</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
            </div>
                <button class="btn btn-primary btn-lg btn-block" name="tambahUser" type="submit">Tambah Data</button>
                <button class="btn btn-danger btn-lg btn-block" type="reset" onclick="return confirm('Apakah anda yakin untuk me-reset data tersebut?')">Reset</button>
                <a class="btn btn-secondary btn-lg btn-block" href="<?= base_url('User')  ?>" >Kembali</a>
          </form>
          </div>
        </div>
      </div>
    </main>