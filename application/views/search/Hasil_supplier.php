  <main role="main">
	  <div class="container-fluid">
		<div class="card">
		  <div class="card-body">
		  	<h4>Supplier Database</h4>
			<div class="float-right pb-5">
				<a class="btn btn-outline-info" href="<?= base_url('barang')?>">Barang</a>
				<a class="btn btn-outline-info" href="<?= base_url('User')?>">User</a>
        <a href="<?= base_url('supplier')?>"  class="btn btn-outline-success d-flex mt-2 px-4 mx-2">Database Supplier</a>
			</div>
			<form class="form-inline" method="get" action="<?= base_url('supplier/search_supplier')?>">
				<input class="form-control mr-sm-2" type="text" placeholder="Search Supplier" name="cari" required>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
			</form>
			<div class="table-responsive">
			  <table class="table table-bordered">
				<thead>
				  <tr>
				    <td>No</td>
				    <td>Kode Supplier</td>
				    <td>Nama Supplier</td>
				    <td>Alamat Supplier</td>
				    <td>Telp Supplier</td>
            <td>Kota Supplier</td>
            <td>Setting</td>
				  </tr>
				</thead>
				<tbody>
          <?php 
          $no = 1;
          foreach ($supplier as $suppliers) : 
          ?>
            <tr>
              <td><?= $no++ ?></td> 
              <td><?= $suppliers['kode_supplier'] ?></td> <!--Kode Supplier-->
              <td><?= $suppliers['nama_supplier'] ?></td> <!--Nama Supplier -->
              <td><?= $suppliers['alamat_supplier'] ?></td> <!--Lokasi Supplier -->
              <td><?= $suppliers['telp_supplier'] ?></td> <!-- Telp Supplier -->
              <td><?= $suppliers['kota_supplier'] ?></td> <!-- Kota Supplier -->
              <td>
                <div class="btn-group">
                  <a class="btn btn-outline-info" href="<?= base_url()?>Supplier/edit_supplier/<?= $suppliers['kode_supplier']; ?>">Edit</a>
                  <a class="btn btn-outline-danger" href="<?= base_url()?>Supplier/hapus_supplier/<?= $suppliers['kode_supplier']; ?>" onclick="confirm('Apakah anda yakin menghapus data dengan nama <?= $suppliers['nama_supplier']; ?>?');">Hapus</a>
                </div>
              </td>
            </tr>
          <?php endforeach; ?>
				</tbody>
			  </table>
			</div>
		  </div>
		</div>
	  </div>
	</main>