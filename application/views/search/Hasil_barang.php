    <main role="main">
      <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <h4>Barang</h4>
            <div class="float-right" style="padding-bottom:20px;">
              <a class="btn btn-outline-info" href="<?= base_url('User')?>">User</a>
              <a class="btn btn-outline-info" href="<?= base_url('supplier')?>">Supplier</a>
              <a href="<?= base_url('barang')?>"  class="btn btn-outline-success d-flex mt-2 px-4 mx-2">Database Barang</a>
            </div>
            <!-- SEARCH -->
            <form class="form-inline" method="" action="<?= base_url('barang/search_barang')  ?>">
              <input class="form-control mr-sm-2" type="text" placeholder="Search Barang" name="cari" required>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
            </form>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Kode Barang</td>
                    <td>Nama Barang</td>
                    <td>Kategori</td>
                    <td>Jenis Barang</td>
                    <td>Lokasi Barang</td>
                    <td>Jumlah Barang</td>
                    <td>Sumber Dana</td>
                    <td>Kondisi</td>
                    <td>Spesifikasi</td>
                    <td>Setting</td>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $no = 1;
                foreach ($barang as $bar) : 
                ?>
                        <tr>
                          <td><?=$no++?></td>
                          <td><?=$bar['kode_barang']?></td> <!--Kode Barang-->
                          <td><?=$bar['nama_barang']?></td> <!--Nama Barang-->
                          <td><?=$bar['kategori']?></td> <!--Kategori -->
                          <td><?=$bar['jenis_barang']?></td> <!--Jenis Barang-->
                          <td><?=$bar['lokasi_barang']?></td> <!--Lokasi Barang-->
                          <td><?=$bar['total_barang']?></td> <!--Jumlah Barang-->
                          <td><?=$bar['sumber_dana']?></td> <!--Sumber Dana-->
                          <td> <!-- Kondisi -->
                            <?php
                            if ($bar['kondisi'] == 0) {
                              echo "<span class='badge badge-info'>Kurang Baik</span>";
                            }
                            else {
                              echo "<span class='badge badge-success'>Baik</span>";
                            }
                           ?></td>
                          <td><?=$bar['spesifikasi']?></td> <!--Spesifikasi-->
                          <td>
                            <div class="btn-group">
                              <a class="btn btn-outline-info" href="<?= base_url()?>barang/edit_barang/<?= $bar['kode_barang']; ?>">Edit</a>
                              <a class="btn btn-outline-danger" href="<?= base_url()?>barang/hapus_barang/<?= $bar['kode_barang']; ?>" onclick="return confirm('Apakah anda yakin menghapus data dengan nama <?=$bar['nama_barang']?>?')">Hapus</a>
                            </div>
                          </td>
                        </tr>
                <?php endforeach; ?>                     
                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </main>